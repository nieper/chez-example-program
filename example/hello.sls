#!r6rs

(library (example hello)
  (export
    hello)
  (import
    (rnrs))

  (define hello
    (lambda ()
      (display "Hello, World!")
      (newline))))
