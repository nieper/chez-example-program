#!/usr/bin/env scheme-script
#!r6rs

(import
 (example hello)
 (rnrs)
 (rnrs eval))

(hello)
(display "> ")
(display (eval (read) (environment '(rnrs))))
(newline)
